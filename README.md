# IPL

Playing with Array of Objects
The following Questions are coded in this project and results have been stored in the outpur Directory

1. Number of matches played per year for all the years in IPL.
2. Number of matches won of per team per year in IPL.
3. Extra runs conceded per team in 2016
4. Top 10 economical bowlers in 2015
5. (Find player per season who has won the highest number of Player of the Match awards)
6. (Find the strike rate of the batsman Virat Kohli for each season)
7. (Find the highest number of times one player has been dismissed by another player)
8. (Find the bowler with the best economy in super overs)