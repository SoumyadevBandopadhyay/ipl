function transformer(){
    const fs=require('fs')
    const number_of_matches_played_per_year= require('../Function_modules/matches_played_peryear.js')
    const number_of_victory_perteam_peryear=require('../../src/Function_modules/number_of_victory_perteam_peryear.js')
    const extras_perteam_2016=require('../../src/Function_modules/extras_perteam_2016.js')
    const top_ten_economical_bowler2015=require('../../src/Function_modules/top_ten_economical_bowler2015.js')
    const own_toss_own_match_by_team=require('../../src/Function_modules/OwnTossOwnMatchByTeam.js')
    const highest_player_of_the_match_by_season=require('../../src/Function_modules/BySeasonPlayerHighestPlayerOfTheMatch.js')
    const virat_kohli_strike_rate_by_season=require('../../src/Function_modules/ViratKohliStrikeRateBySeason.js')
    const highest_number_of_times_one_player_dismissed_by_another=require('../../src/Function_modules/HighestNumberTimesOneBatsmanDismissedByBowler.js')
    const bowler_with_best_eco_super_over=require('../../src/Function_modules/BowlerWithBestEcoSuperOver.js')


    const csvFilePath='./src/data/matches.csv'
    const csv=require('csvtojson')
    csv()
    .fromFile(csvFilePath)
    .then((matchData)=>{
        const csvFilePath1='./src/data/deliveries.csv'
        csv()
        .fromFile(csvFilePath1)
        .then((deliveryData)=>{
            let NMPPY=number_of_matches_played_per_year(matchData)
            let NVPTPY=number_of_victory_perteam_peryear(matchData,deliveryData)
            let EPT2016=extras_perteam_2016(matchData,deliveryData)
            let TTEP=top_ten_economical_bowler2015(matchData,deliveryData)
            let OTOMBT=own_toss_own_match_by_team(matchData)
            let HPOTMBS=highest_player_of_the_match_by_season(matchData)
            let VKSRBS=virat_kohli_strike_rate_by_season(matchData,deliveryData)
            let HNOTOPDBA=highest_number_of_times_one_player_dismissed_by_another(deliveryData)
            let BWBESO=bowler_with_best_eco_super_over(deliveryData)

            console.log("********************************************")
            console.log("THE NUMBER OF MATCHES PLAYED PER YEAR:")
            console.log("*******************************************\n")
            console.log(NMPPY)
            console.log("\n\n*******************************************")
            console.log('THE NUMBER OF VICTORY PERTEAM PER YEAR:')
            console.log('********************************************')
            console.log(NVPTPY)
            console.log("\n\n********************************************")
            console.log('EXTRAS CONCEDED PER TEAM IN 2016:')
            console.log('**************************************************')
            console.log(EPT2016)
            console.log("\n\n*************************************************")
            console.log('TOP TEN ECONOMICAL BOWLER OF 2015:')
            console.log("****************************************************")
            console.log(TTEP)
            console.log("\n\n*************************************************")
            console.log("HIGHEST PLAYER OF THE MATCH AWARD RECIEVED BY SEASON:")
            console.log("******************************************************")
            console.log(HPOTMBS)
            console.log("\n\n****************************************************")
            console.log("NUMBER OF TIMES EACH TEAM OWN THE TOSS AND  OWN THE MATCH:")
            console.log("***********************************************************")
            console.log(OTOMBT)
            console.log("\n\n******************************************************\nSTRIKE RATE OF VIRAT KOHLI BY SEASON:\n****************************************************************")
            console.log(VKSRBS)
            console.log("\n\n*********************************************************")
            console.log("THE HIGHEST NUMBER OF TIMES ONE PLAYER DISMISSED BY ANOTHER")
            console.log("*****************************************************************")
            console.log(HNOTOPDBA)
            console.log("\n\n********************************************************")
            console.log("THE BOWLER HAVING THE BEST ECONOMY IN SUPER OVER:")
            console.log("***************************************************************")
            console.log(BWBESO)
            
            // fs.writeFile('../output/matchesPerYear.json',JSON.stringify(NMPPY),'utf8',(err) => {
            //     if (err) throw err;
            //     console.log('Data written to file');
            // })
            // fs.writeFile('../output/victoryPerTeamPerYear.json',JSON.stringify(NVPTPY),(err) => {
            //     if (err) throw err;
            //     console.log('Data written to file');
            // })
            // fs.writeFile('../output/extrasConceded.json',JSON.stringify(EPT2016),(err) => {
            //     if (err) throw err;
            //     console.log('Data written to file');
            // })
            // fs.writeFile('../output/topTenBowler.json',JSON.stringify(TTEP),(err) => {
            //     if (err) throw err;
            //     console.log('Data written to file');
            // })
            // fs.writeFile('../output/ownTossOwnMatchByTeam.json',JSON.stringify(OTOMBT),(err) => {
            //          if (err) throw err;
            //          console.log('Data written to file');
            // })
            //  fs.writeFile('../output/highestPlayerOfTheMatchBySeason.json',JSON.stringify(HPOTMBS),(err) => {
            //         if (err) throw err;
            //         console.log('Data written to file');
            // })
            // fs.writeFile('../output/viratKohliStrikeRateBySeason.json',JSON.stringify(VKSRBS),(err) => {
            //     if (err) throw err;
            //     console.log('Data written to file');
            // })
            // fs.writeFile('../output/highestNumberOfTimesOnePlayerDismissedByAnother.json',JSON.stringify(HNOTOPDBA),(err) => {
            //     if (err) throw err;
            //     console.log('Data written to file');
            // })
            // fs.writeFile('../output/bowlerWithBestEcoSuperOver.json',JSON.stringify(BWBESO),(err) => {
            //     if (err) throw err;
            //     console.log('Data written to file');
            // })
        
        })

    })
}
module.exports=transformer