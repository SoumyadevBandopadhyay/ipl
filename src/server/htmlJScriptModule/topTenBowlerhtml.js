var xml=new XMLHttpRequest()
xml.open('GET', '/topTenBowler.json', true)
xml.send()
//xml.responseType='json' 
xml.onreadystatechange= function() {
    if(this.readyState == 4 && this.status == 200){
        let data =JSON.parse(xml.response)
        Highcharts.chart('container', {
            chart: {
                type: 'column'
            },
            title: {
                text: 'Top Ten Bowlers Of IPL'
            },
            subtitle: {
                text: 'Top Ten Bolwers with the best economy rate in IPL'
            },
            xAxis: {
                categories: Object.keys(data),
                crosshair: true
            },
            yAxis: {
                min: 0,
                title: {
                    text: 'Economy Rate of the bowler'
                }
            },
            tooltip: {
                headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                pointFormat: '<tr><td style="color:{series.color};padding:0">Economy: </td>' +
                    '<td style="padding:0"><b>{point.y}</b></td></tr>',
                footerFormat: '</table>',
                shared: true,
                useHTML: true
            },
            plotOptions: {
                column: {
                    pointPadding: 0.2,
                    borderWidth: 0
                }
            },
            series: [{name:'Bowler',
                      data: Object.values(data)      
                    }]
        });
    }else if(this.readyState !=4){
        console.log("Loading")
    }
}

xml.onerror=function (e){
    console.error(xml.statusText);
}