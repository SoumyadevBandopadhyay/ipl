var xml=new XMLHttpRequest()
xml.open('GET', '/highestPlayerOfTheMatchBySeason.json', true)
xml.send()
//xml.responseType='json' 
xml.onreadystatechange= function() {
    if(this.readyState == 4 && this.status == 200){
        let data =JSON.parse(xml.response)
        //console.log(Object.keys(data))
        let seriesData=Object.values(data).reduce((acc, curr)=> {
            let obj={}
            obj['name']= Object.keys(curr)[0]
            obj['data']=Array(10).fill(0)
            obj['data'][Object.values(data).indexOf(curr)]= Object.values(curr)[0]
            acc.push(obj)
            return acc
        },[])
        //console.log(seriesData)
        Highcharts.chart('container', {
            chart: {
                type: 'column'
            },
            title: {
                text: 'Highest Player Of The Match By Season'
            },
            subtitle: {
                text: 'The chart of Highest Player of the Match  Receiver Award By Season'
            },
            xAxis: {
                categories: Object.keys(data),  
                crosshair: true
            },
            yAxis: {
                min: 0,
                title: {
                    text: 'Number Of Times Award Received'
                }
            },
            tooltip: {
                headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                    '<td style="padding:0"><b>{point.y}</b></td></tr>',
                footerFormat: '</table>',
                shared: true,
                useHTML: true
            },
            plotOptions: {
                column: {
                    pointPadding: 0.2,
                    borderWidth: 0
                }
            },
            series: seriesData
        });
    }else if(this.readyState !=4){
        console.log("Loading")
    }
}

xml.onerror=function (e){
    console.error(xml.statusText);
}