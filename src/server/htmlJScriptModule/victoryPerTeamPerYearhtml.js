var xml=new XMLHttpRequest()
xml.open('GET', '/victoryPerTeamPerYear.json', true)
xml.send()
//xml.responseType='json' 
xml.onreadystatechange= function() {
    if(this.readyState == 4 && this.status == 200){
        let data =JSON.parse(xml.response)
        let seriesData={}
        for(let year in data){
            for(let eachTeam in data[year]){
                if(seriesData[eachTeam]){
                    seriesData[eachTeam][year]=data[year][eachTeam]
                }else{
                    seriesData[eachTeam]={}
                    seriesData[eachTeam][year]=data[year][eachTeam]
                }
            }
        }
        //console.log(seriesData)
        let seriesDataArr=[]
        for(let eachTeamDataByYear in seriesData){
            let obj={}
            if(eachTeamDataByYear.length!=0){
                obj['name']=eachTeamDataByYear
                let dataYear=[]
                for(let i=2008; i<=2017;i++){
                    dataYear.push(i)
                }
                let yearWin=Object.entries(seriesData[eachTeamDataByYear])
                //console.log(dataYear)
                //i=[Year,Win] [2008,6]
                for(let i of yearWin){
                    //console.log(i[1])
                    dataYear[dataYear.indexOf(Number(i[0]))]=i[1]
                }
                //i=either year or number of wins
                // If the year is not a year and some win value then keep it
                //otherwise zero it.
                //console.log(dataYear)
                for(let i of dataYear){
                    if(JSON.stringify(i).length==4){
                        dataYear[dataYear.indexOf(i)]=null
                    }
                }
                //console.log(dataYear)
                obj['data']=dataYear
                seriesDataArr.push(obj)
            }
        }
        console.log(seriesDataArr)
        Highcharts.chart('container', {
            chart: {
                type: 'column'
            },
            title: {
                text: 'Victory Count Of Teams By Season'
            },
            subtitle: {
                text: 'The Number Of Victories Achived By All Teams In Each Season'
            },
            xAxis: {
                categories: Object.keys(data),
                crosshair: true
            },
            yAxis: {
                min: 0,
                title: {
                    text: 'Number Of Victories'
                }
            },
            tooltip: {
                headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                    '<td style="padding:0"><b>{point.y}</b></td></tr>',
                footerFormat: '</table>',
                shared: true,
                useHTML: true
            },
            plotOptions: {
                column: {
                    pointPadding: 0.2,
                    borderWidth: 0
                }
            },
            series: seriesDataArr
        });
    }else if(this.readyState !=4){
        console.log("Loading")
    }
}

xml.onerror=function (e){
    console.error(xml.statusText);
}