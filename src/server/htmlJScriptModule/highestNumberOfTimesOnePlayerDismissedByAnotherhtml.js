var xml=new XMLHttpRequest()
xml.open('GET', '/highestNumberOfTimesOnePlayerDismissedByAnother.json', true)
xml.send()
//xml.responseType='json' 
xml.onreadystatechange= function() {
    if(this.readyState == 4 && this.status == 200){
        let data =JSON.parse(xml.response)
        let seriesData=[] // The data we want to pass in the serries attribute of the  highchart.
        let seriesDataObj={} // The object we want to push to seriesData.
        seriesDataObj.name="Batsman"
        seriesDataObj.colorByPoint=true
        seriesDataObj.data=[]
        for(let eachBatsman in data){
            let obj={} // The object we want to push in seriesDataObj.data array
            obj.name=eachBatsman
            obj.y=Object.values(data[eachBatsman])[0]
            obj.drilldown=eachBatsman
            seriesDataObj.data.push(obj)
        }
        seriesData.push(seriesDataObj)
        let drilldownSeries=[] //The Array we want to pass in the drill down series.
        for(let eachBatsman in data){
            let obj={} //The each object we want to pass in drilldownSeries.
            obj.name=eachBatsman
            obj.id=eachBatsman
            obj.data=Object.entries(data[eachBatsman])
            drilldownSeries.push(obj)
        }

        Highcharts.chart('container', {
            chart: {
                type: 'column'
            },
            title: {
                text: 'Highest Number Of Times One Player Dismissed By Another.'
            },
            subtitle: {
                text: 'Click the columns to view the bowlers who dismissed this player.</a>'
            },
            accessibility: {
                announceNewData: {
                    enabled: true
                }
            },
            xAxis: {
                type: 'category'
            },
            yAxis: {
                title: {
                    text: 'Number Of Times Dismissed.'
                }

            },
            legend: {
                enabled: false
            },
            plotOptions: {
                series: {
                    borderWidth: 0,
                    dataLabels: {
                        enabled: true,
                        format: '{point.y}'
                    }
                }
            },

            tooltip: {
                headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
                pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y}'
            },

            series: seriesData,
            drilldown: {
                series: drilldownSeries 
            }
        });
    }else if(this.readyState !=4){
        console.log("Loading")
    }
}

xml.onerror=function (e){
    console.error(xml.statusText);
}