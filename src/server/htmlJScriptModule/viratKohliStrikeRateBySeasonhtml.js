var xml=new XMLHttpRequest()
xml.open('GET', '/viratKohliStrikeRateBySeason.json', true)
xml.send()
//xml.responseType='json' 
xml.onreadystatechange= function() {
    if(this.readyState == 4 && this.status == 200){
        let data =JSON.parse(xml.response)
        let seriesData=Object.values(data).reduce((acc, curr)=> {
                acc.push(curr["Strike-Rate"])
                return acc
        },[])
        console.log(seriesData)
        Highcharts.chart('container', {
            chart: {
                type: 'column'
            },
            title: {
                text: 'Virat Kohli Strike Rate'
            },
            subtitle: {
                text: 'Strike Rate Of Virat Kohli By Season'
            },
            xAxis: {
                categories: Object.keys(data),
                crosshair: true
            },
            yAxis: {
                min: 0,
                title: {
                    text: 'Strike Rate'
                }
            },
            tooltip: {
                headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                pointFormat: '<tr><td style="color:{series.color};padding:0">Strike Rate: </td>' +
                    '<td style="padding:0"><b>{point.y}</b></td></tr>',
                footerFormat: '</table>',
                shared: true,
                useHTML: true
            },
            plotOptions: {
                column: {
                    pointPadding: 0.2,
                    borderWidth: 0
                }
            },
            series: [{name:'Virat Kohli',
                        data: seriesData     
                    }]
        });
    }else if(this.readyState !=4){
        console.log("Loading")
    }
}
xml.onerror=function (e){
    console.error(xml.statusText);
}