const http = require ('http')
const fs = require ('fs')
http.createServer ((request, response)=> {
    let path= request.url.slice(-1)
    switch(path){
        case '/' :
            response.writeHead(200, {'Content-Type': 'text/html'})
            fs.readFile('./index.html', 'utf-8', (err,data) =>{
                if(err){
                    console.error("Can't process a error has been encounterd")
                    response.end()
                }else{
                    response.write(data)
                    response.end()
                }
            })
            break
        case 'n' :
            response.writeHead(200, {'Content-Type': 'text/json'})
            fs.readFile('../output'+request.url, 'utf-8', (err,data) =>{
                if(err){
                    console.error("Can't process an error has been encounterd")
                    response.end()
                }else{
                    response.write(data)
                    response.end()
                }
            })
            break
                
        case 'l' :
            response.writeHead(200, {'Content-Type': 'text/html'})
            fs.readFile('./IplHtmls'+request.url, 'utf-8', (err,data) =>{
                if(err){
                    console.log("Can't process an error has been encountered")
                    response.end()
                }else{
                    response.write(data)
                    response.end()
                }
            })
            break
        
        case 's' :
            response.writeHead(200, {'Content-Type': 'text/javascript'})
            fs.readFile('./htmlJScriptModule'+request.url , 'utf-8', (err,data) =>{
                if(err){
                    console.err("Can't process an error has been encountered",err)
                    response.end()
                }else{
                    response.write(data)
                    response.end()
                }
            })
            break

        default :
            response.writeHead(404, {'Content-Type': 'text/html'})
            fs.readFile('./error.html', 'utf-8',(err, data)=> {
                if(err){
                    console.err("Can't find root to error message")
                    response.end()
                }else{
                    response.write(data)
                    response.end()
                }
            })
            
    }

}).listen(7009)