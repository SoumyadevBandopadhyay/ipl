// const fs=require('fs')
// let matchData=fs.readFileSync('./matches.json')
// matchData=JSON.parse(matchData)
/****************************
 * Funtion to find the number of times each team won the toss and the match as well.
 */

function OwnTossOwnMatch(matchData){
    let obj={}
    matchData.filter(({toss_winner,winner})=>{
        if(toss_winner==winner){
            if(obj[winner]){
                obj[winner]++
            }else{
                obj[winner]=1
            }
        }
    })
    return obj
}
module.exports=OwnTossOwnMatch


