// const fs=require('fs')
// let deliveryData=fs.readFileSync('./deliveries.json')
// deliveryData=JSON.parse(deliveryData)

function bowler_with_best_eco_super_over(deliveryData){
    let obj={}
    deliveryData.filter(({is_super_over,bowler,wide_runs,noball_runs,total_runs,legbye_runs,bye_runs})=>{
        if(is_super_over!=0){
            if(obj[bowler]){
                if(wide_runs!=0 || noball_runs!=0){
                    if(obj[bowler]['total']){
                        obj[bowler]['total']+=total_runs-legbye_runs-bye_runs
                    }else{
                        obj[bowler]['total']=total_runs-legbye_runs-bye_runs
                    }
                }else{
                    if(obj[bowler]['total']){
                        obj[bowler]['total']+=total_runs-legbye_runs-bye_runs
                    }else{
                        obj[bowler]['total']=total_runs-legbye_runs-bye_runs
                    }
                    if(obj[bowler]['balls']){
                        obj[bowler]['balls']++
                    }else{
                        obj[bowler]['balls']=1
                    }
                }

            }else{
                obj[bowler]={}
                if(wide_runs!=0 || noball_runs!=0){
                    if(obj[bowler]['total']){
                        obj[bowler]['total']+=total_runs-legbye_runs-bye_runs
                    }else{
                        obj[bowler]['total']=total_runs-legbye_runs-bye_runs
                    }
                }else{
                    if(obj[bowler]['total']){
                        obj[bowler]['total']+=total_runs-legbye_runs-bye_runs
                    }else{
                        obj[bowler]['total']=total_runs-legbye_runs-bye_runs
                    }
                    if(obj[bowler]['balls']){
                        obj[bowler]['balls']++
                    }else{
                        obj[bowler]['balls']=1
                    }
                }
            }
        }
    })
    let obj2={}
    for(let bowler in obj){
        let overs=Math.floor(obj[bowler]['balls']/6)+(obj[bowler]['balls']%6)*(1/6)
        obj2[bowler]=(obj[bowler]['total']/overs).toFixed(2)
    }
    let min=Math.min(...Object.values(obj2))
    let finalObj={}
    for(let bowler in obj2){
        if(obj2[bowler]==min){
            finalObj[bowler]=min
        }
    }
    return finalObj
}

module.exports=bowler_with_best_eco_super_over