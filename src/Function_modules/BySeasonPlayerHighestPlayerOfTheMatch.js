// const fs=require('fs')
// let matchData=fs.readFileSync('./matches.json')
// matchData=JSON.parse(matchData)
/************************
 * Function to find the highest winner of Player of the Match Award by Season
 */

 function highestPlayerOfMatchPlayerBySeason(matchData){
     let obj={}
     matchData.filter(({season,player_of_match})=>{
         if(obj[season]){
            if(obj[season][player_of_match]){
                obj[season][player_of_match]++
            }else{
                obj[season][player_of_match]=1
            }
         }else{
             obj[season]={}
             obj[season][player_of_match]=1
         }
     })
     let outputObj={}
     for(let year in obj){
        let max=Math.max(...Object.values(obj[year]))
        for(let player in obj[year]){
            if(outputObj[year]){
                if(obj[year][player]==max){
                    outputObj[year][player]=max
                }
            }else{
                outputObj[year]={}
                if(obj[year][player]==max){
                    outputObj[year][player]=max
                }
            }
            
        }
     }
     return outputObj
 }
 module.exports=highestPlayerOfMatchPlayerBySeason
