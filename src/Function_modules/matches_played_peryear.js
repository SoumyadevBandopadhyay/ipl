/*******************
 * Function to determine the number of matches played per year.
 */

function matchesPlayedPerYear(matchArr){
    let requiredObj={}
    for(let eachMatchStat of matchArr){
        if(requiredObj[eachMatchStat['season']] != undefined){
            requiredObj[eachMatchStat['season']] ++
        }else{
            requiredObj[eachMatchStat['season']]=1
        }
    }
    return requiredObj
}

module.exports=matchesPlayedPerYear


