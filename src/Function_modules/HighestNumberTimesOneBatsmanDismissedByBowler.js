// const fs=require('fs')
// let deliveryData=fs.readFileSync('./deliveries.json')
// deliveryData=JSON.parse(deliveryData)
// console.log(deliveryData)
/***********************************************
 * Function to Calculate the Highest Number of Times One Batsman was dismissed by a bowler
 */

 function highest_number_of_times_one_batsman_dismissed_by_a_bowler(deliveryData){
     let obj={}
     deliveryData.filter(({bowler,player_dismissed})=>{
         if(player_dismissed.length!=0){
            if(obj[player_dismissed]){
                if(obj[player_dismissed][bowler]){
                    obj[player_dismissed][bowler]++
                }else{
                    obj[player_dismissed][bowler]=1
                }
            }else{
                obj[player_dismissed]={}
                if(obj[player_dismissed][bowler]){
                    obj[player_dismissed][bowler]++
                }else{
                    obj[player_dismissed][bowler]=1
                }
            }
         }
     })
     let output={}
     for(let batsman in obj){
         let max=Math.max(...Object.values(obj[batsman]))
         for(let bowler in obj[batsman]){
             if(obj[batsman][bowler]==max){
                 if(output[batsman]){
                    output[batsman]['Striked out by '+bowler]=max
                 }else{
                     output[batsman]={}
                     output[batsman]['Striked out by '+bowler]=max
                 }
             }
         }
     }
     return output
 }

 module.exports=highest_number_of_times_one_batsman_dismissed_by_a_bowler