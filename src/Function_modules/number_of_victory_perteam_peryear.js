/****************
 * Function to print the number of victories achieved by each team each year.
 */

function victoryTeamYearStat(matchRecords){
    let victoryStat={}
    matchRecords.filter(({season,winner})=>{
        if(victoryStat[season]){
            if(victoryStat[season][winner]){
                victoryStat[season][winner]++
            }else{
                victoryStat[season][winner]=1
            }
        }else{
            victoryStat[season]={}
            victoryStat[season][winner]=1
        }
    })
    return victoryStat
} 
module.exports=victoryTeamYearStat
