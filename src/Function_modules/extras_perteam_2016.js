/**************
 * Funtion to return the extra runs conceded by each team in year 2016.
 */
function extraConceded(match,delivery){
    let extraByTeam={}
    match.filter(({id,season})=>{
        if(season=='2016'){
            delivery.filter(({match_id,bowling_team,extra_runs})=>{
                if(id==match_id){
                    if(extraByTeam[bowling_team]){
                        extraByTeam[bowling_team]+=Number(extra_runs)
                    }else{
                        extraByTeam[bowling_team]=Number(extra_runs)
                    }
                }
            })
        }
    })    
return extraByTeam
}
module.exports=extraConceded

    
