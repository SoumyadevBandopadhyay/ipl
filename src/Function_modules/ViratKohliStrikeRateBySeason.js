// const fs=require('fs')
// let matchData=fs.readFileSync('./matches.json')
// let deliveryData=fs.readFileSync('./deliveries.json')
// matchData=JSON.parse(matchData)
// deliveryData=JSON.parse(deliveryData)
//console.log(deliveryData)
/*******************************
 * Function to compute the Strike Rate of Virat Kohli in each season of IPL
 */

function ViratKohliStrikeRateBySeason(matchData,deliveryData){
    let obj={}
    matchData.filter(({season,id})=>{
        deliveryData.filter(({match_id,batsman,wide_runs,noball_runs,batsman_runs})=>{
            if(id==match_id && batsman=="V Kohli"){
                if(obj[season]){
                    obj[season][batsman]+=Number(batsman_runs)
                    if(wide_runs!=0&&noball_runs!=0){

                    }else{
                        obj[season]['balls']++
                    }
                }else{
                    obj[season]={}
                    if(obj[season][batsman]){
                        obj[season][batsman]+=Number(batsman_runs)
                    }else{
                        obj[season][batsman]=Number(batsman_runs)
                    }
                    if(noball_runs!=0&& wide_runs!=0){
                    
                    }else{
                        if(obj[season]['balls']){
                            obj[season]['balls']++
                        }else{
                            obj[season]['balls']=1
                        }
                    }
                }    
            }
        })
    })
    for(let year in obj){
        obj[year]['Strike-Rate']=Number((obj[year]['V Kohli']/obj[year]['balls']*100).toFixed(3))
    }
    return obj
}

module.exports=ViratKohliStrikeRateBySeason


