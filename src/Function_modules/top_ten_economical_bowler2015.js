/***************
 *Funtion to display the top ten Economic bowlers. 
 */

 function topTenEcoBowler(matchData,delData){
    let resultObj={}
    matchData.filter(({id,season})=>{
        if(season==2015){
            delData.filter(({match_id,bowler,total_runs,wide_runs,noball_runs,bye_runs,legbye_runs})=>{
                if(id==match_id){
                    if(resultObj[bowler]){
                        resultObj[bowler]["total"]+=total_runs-bye_runs-legbye_runs   
                        if(noball_runs!=0||wide_runs!=0){
                            // No change in ball count
                        }else{
                            resultObj[bowler]["balls"]++
                        }
                    }else{
                        resultObj[bowler]={}
                        resultObj[bowler]["total"]=total_runs-bye_runs-legbye_runs
                        if(noball_runs!=0||wide_runs!=0){
                            if(resultObj[bowler]["balls"]){
                                resultObj[bowler]["balls"]+=0
                            }else{
                                resultObj[bowler]["balls"]=0
                            }
                        }else{
                                if(resultObj[bowler]["balls"]){
                                    resultObj[bowler]["balls"]++
                                }else{
                                    resultObj[bowler]["balls"]=1
                                }
                        }

                    }
                }
            })
        }
    })

    let bowlerEcomonyObj={}
    for(let bowler in resultObj){
        let overs=(resultObj[bowler]['balls']%6)*(1/6) + Math.floor(resultObj[bowler]['balls']/6)
        let economy=resultObj[bowler]['total']/overs
        economy=economy.toFixed(2)
        bowlerEcomonyObj[bowler]=Number(economy)
    }
    let bowlerEcoArr=Object.entries(bowlerEcomonyObj).sort((a,b)=>a[1]-b[1])
    let topTenEcoBowler2015=bowlerEcoArr.slice(0,10)
    let topTen={}
    for(let i of topTenEcoBowler2015){
        topTen[i[0]]=i[1]
    }
    return topTen
}
module.exports=topTenEcoBowler


